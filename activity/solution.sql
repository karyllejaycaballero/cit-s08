-- Karylle Jay H. Caballero
-- S08 Activity

-- 2.A All artists that have d in their name
	SELECT * FROM artists WHERE name LIKE "%d%";

-- 2.B All songs that has a length of less than 230
	SELECT * FROM songs WHERE length < 230;

-- 2.C Join the albums and songs tables show only album name, song_name, and song length
	SELECT album_title, song_name, length FROM albums
	JOIN songs ON albums.id = songs.album_id;

-- 2.D Join the artists and album tables (Find all albums that have has a letter A in its name)
	SELECT * FROM albums
	JOIN artists ON albums.artists_id = artists.id WHERE album_title LIKE "%A%";

-- 2.E Sort the albums in Z-A order show only the first 4 records
	SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- 2.F Join the albums and songs tables. Sort albums from Z-A and sort songs from A-Z).
	SELECT *  FROM albums
	JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC, song_name ASC;

